#import "NSString+MD5.h"

@implementation NSString(MD5)

- (NSString *)urlencode {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[self UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

-(NSString *)htmlDecode
{
    NSString * output = [self stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    output = [output stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    output = [output stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    output = [output stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    return output;
}

@end