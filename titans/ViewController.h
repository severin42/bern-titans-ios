//
//  ViewController.h
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface ViewController : UITabBarController<UITextFieldDelegate>
{
    DBManager * manager;
}

- (void) refresh:(id)sender;
-(void)showLoginScreen;
-(void)hideLoginScreen;

@end

