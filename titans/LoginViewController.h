//
//  LoginViewController.h
//  titans
//
//  Created by Severin Zumbrunn on 08.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate,NSURLConnectionDelegate,UITextViewDelegate,UIAlertViewDelegate>
{
    UITextField * nameField;
    UITextField * pwField;
    UISegmentedControl * segmentedControl;
    
    UIButton * resetpw;
    
    UIView * containerView;
    NSMutableData * connData;
    NSURLConnection * conn;
    
    NSString * username;
    NSString * password;
    int type;
    
    UITextView * fView;
}
-(void) showRegisterView:(id)sender;
-(void) login:(id)sender;
-(void) resetPassword:(id)sender;
-(void)onKeyboardShow:(NSNotification *)notification;
-(void)onKeyboardHide:(NSNotification *)notification;

@end
