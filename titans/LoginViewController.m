//
//  LoginViewController.m
//  titans
//
//  Created by Severin Zumbrunn on 08.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "AppDelegate.h"

#define APP_URL @"http://bernlacrosse.ch/app/app.php"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    

    
    // Do any additional setup after loading the view.
    CGSize screen = [[UIScreen mainScreen] bounds].size;
    
    int height_pos = screen.height/2 - 170;
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen.width, screen.height)];
    [containerView setBackgroundColor:[UIColor clearColor]];

    nameField = [[UITextField alloc] initWithFrame:CGRectMake(20, 90+height_pos, screen.width-40, 30)];
    [nameField setTag:1];
    nameField.borderStyle = UITextBorderStyleRoundedRect;
    [nameField setKeyboardType:UIKeyboardTypeEmailAddress];
    [nameField setReturnKeyType:UIReturnKeySend];
    pwField = [[UITextField alloc] initWithFrame:CGRectMake(20, 160+height_pos, screen.width-40, 30)];
    [pwField setTag:1];
    pwField.borderStyle = UITextBorderStyleRoundedRect;
    [pwField setKeyboardType:UIKeyboardTypeAlphabet];
    [pwField setSecureTextEntry:YES];
    [pwField setReturnKeyType:UIReturnKeySend];
    [pwField addTarget:self action:@selector(login:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    segmentedControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Men", @"Women", @"None",nil]];
    [segmentedControl setFrame:CGRectMake(20, 230+height_pos, screen.width-40, 30)];
    [segmentedControl setSelectedSegmentIndex:-1];
    
    fView = [[UITextView alloc] initWithFrame:CGRectMake(20, 290+height_pos, screen.width-40, 120)];
    [fView setDataDetectorTypes:UIDataDetectorTypeAll];
    
    [fView setText:@"Did you forget your password? Click the following link http://bernlacrosse.ch/index.php?option=com_users&view=reset"];
    [fView setEditable:NO];
    [fView setUserInteractionEnabled:YES];
    [fView setDelegate:self];
    [fView setBackgroundColor:[UIColor clearColor]];
    [fView setFont:[UIFont fontWithName:@"Helvetica" size:16.0f]];
    [fView setHidden:true];
    
    resetpw = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [resetpw setFrame:CGRectMake(23, 350+height_pos, 120, 40)];
    [resetpw setHidden:true];
    
    [resetpw setTitle:@"Reset Password" forState:UIControlStateNormal];
    [resetpw addTarget:self action:@selector(resetPassword:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel * name = [[UILabel alloc] initWithFrame:CGRectMake(20, 60+height_pos, 200, 30)];
    [name setText:@"Enter your email:"];
    [name setTag:1];
    UILabel * pw = [[UILabel alloc] initWithFrame:CGRectMake(20, 130+height_pos, 200, 30)];
    [pw setText:@"Enter your password:"];
    [pw setTag:1];
    UILabel * typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 200+height_pos, screen.width-40, 30)];
    [typeLabel setText:@"Choose your Team:"];
    UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setFrame:CGRectMake(20, 260+height_pos, 60, 40)];
    [button setTag:1];
    [button setTitle:@"Submit" forState:UIControlStateNormal];
    //[button setFont:[UIFont fontWithName:@"Helvetica" size:16.0f]];
    [button addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel * quest = [[UILabel alloc] initWithFrame:CGRectMake(20, 30, 200, 30)];
    [quest setText:@"Don't have an account yet? "];
    [quest setFont:[UIFont fontWithName:@"Helvetica" size:16.0f]];
    [quest setTag:1];
    
    UIButton * registerButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [registerButton setFrame:CGRectMake(230, 25, 60, 40)];
    [registerButton setTag:2];
    
    [registerButton setTitle:@"Register" forState:UIControlStateNormal];
    [registerButton addTarget:self action:@selector(showRegisterView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView * imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.jpg"]];
    [imgView setFrame:CGRectMake(0, 60, screen.width, screen.height-120)];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    [imgView setAlpha:0.05f];
    [self.view addSubview:imgView];
    [containerView addSubview:typeLabel];
    [containerView addSubview:nameField];
    [containerView addSubview:pwField];
    [containerView addSubview:name];
    [containerView addSubview:pw];
    [containerView addSubview:button];
    [containerView addSubview:segmentedControl];
    [containerView addSubview:registerButton];
    [containerView addSubview:fView];
    [containerView addSubview:quest];
    [containerView addSubview:resetpw];
    [self.view addSubview:containerView];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)onKeyboardShow:(NSNotification *)notification
{
    CGSize screen = [[UIScreen mainScreen] bounds].size;
    CGRect frame = CGRectMake(0, -screen.height/3+45, screen.width, screen.height);
    [UIView animateWithDuration:0.5
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         containerView.frame = frame;
                         
                     }
                     completion:^(BOOL finished){
                     }];
}

-(void)onKeyboardHide:(NSNotification *)notification
{
    CGSize screen = [[UIScreen mainScreen] bounds].size;
    CGRect frame = CGRectMake(0, 0, screen.width, screen.height);
    [UIView animateWithDuration:0.5
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         containerView.frame = frame;
                     }
                     completion:^(BOOL finished){
                     }];
}

-(void) showRegisterView:(id)sender
{
    RegisterViewController * regView = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:regView animated:YES];
}

-(void) login:(id)sender
{
    [fView setHidden:true];
    [self.view endEditing:YES];

    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
    [request setHTTPMethod:@"POST"];
    username = [nameField text];
    password = [pwField text];
    type = [segmentedControl selectedSegmentIndex];
    if(type!=-1)
    {
        NSString *postString = [NSString stringWithFormat:@"name=%@&pw=%@", [username urlencode],[password urlencode]];
    // NSLog(@"send token: %@", postString);
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        NSLog(@"send: %@", postString);
        connData = nil;
        conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    else
    {
        [fView setText:@"Please enter your credentials and select your team."];
        [fView setHidden:false];
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection error!");
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection Error" message:@"Please check your internet connection and try again." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(self->connData==nil)
        self->connData = [[NSMutableData alloc] init];
    [self->connData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
}

-(void) resetPassword:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Password reset" message:@"Do you really want to reset your password?" preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //Handel your yes please button action here
                                    // if okay was pressed
                                    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
                                    [request setHTTPMethod:@"POST"];
                                    NSString *postString = [NSString stringWithFormat:@"email=%@&type=resetpassword", [[nameField text] urlencode]];
                                    // NSLog(@"send token: %@", postString);
                                    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
                                    NSLog(@"send: %@", postString);
                                    connData = nil;
                                    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                                    
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //Handel no, thanks button
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [resetpw setHidden:true];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:connData options:0 error:&localError];
    int authentication = [[parsedObject valueForKey:@"authenticated"] integerValue];
    int pwreset = [[parsedObject valueForKey:@"pwreset"] integerValue];
    int status = [[parsedObject valueForKey:@"status"] integerValue];
    NSLog(@"Login: %d", status);
    
    if(authentication==1)
    {
        NSLog(@"selected index is: %d",[segmentedControl selectedSegmentIndex]);
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate setCredentialsWithUsername:username andPassword:password andType:[segmentedControl selectedSegmentIndex]];
        [appDelegate registerForNotifications];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        if(pwreset==1)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Password reset successful" message:@"Please check your email." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                
            }];
            
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];

        }
        else if(pwreset==-1)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[parsedObject objectForKey:@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                
            }];
            
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            if(status==-1)
            {
                [fView setText:@"No user with this email found."];
                [fView setHidden:false];
            }
            else if(status==-2)
            {
                [fView setText:@"Your account is not activated. Please contact your administrator"];
                [fView setHidden:false];
            }
            else
            {
                [fView setText:@"Did you forget your password? Click the button below to reset your password."];
                [resetpw setHidden:false];
                [fView setHidden:false];
            }
        }
    }
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    //self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    self.title = NSLocalizedString(@"Login", @"Login");
    //self.tabBarItem.image = [UIImage imageNamed:@"]
    
    return self;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
