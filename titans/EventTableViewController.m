//
//  EventTableViewController.m
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "EventTableViewController.h"
#import "EventTableViewCell.h"
#import "DetailViewController.h"
#import "AppDelegate.h"


@interface EventTableViewController ()

@end

@implementation EventTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    manager = [[DBManager alloc] initWithDatabaseFilename:@"titansdb.sql"];
    
    [self removeOldObjectsFromDB];
    [self loadObjectsFromDB];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //UIBarButtonItem * bt = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(loadObjectsFromDB)];
    //self.navigationItem.rightBarButtonItem = bt;
    
    CGSize screen = [[UIScreen mainScreen] bounds].size;
    bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.jpg"]];
    [bgView setFrame:CGRectMake(0, 60, screen.width*0.9, screen.height-120)];
    [bgView setContentMode:UIViewContentModeScaleAspectFit];
    [bgView setAlpha:0.05f];
    [self.tableView setBackgroundView:bgView];
    
    self.navigationController.navigationBar.tintColor = [UIColor orangeColor]; self.navigationController.navigationBar.translucent = false;
    self.tabBarController.tabBar.tintColor =[UIColor orangeColor];
    self.tabBarController.tabBar.translucent = false;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor whiteColor];
    self.refreshControl.tintColor = [UIColor blackColor];
    [self.refreshControl addTarget:appDelegate
                            action:@selector(loadEvents)
                  forControlEvents:UIControlEventValueChanged];
    
   // UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editFilter:)];
   // self.navigationItem.rightBarButtonItem = anotherButton;
    
    changesWaitingForConfirmation = [[NSMutableArray alloc] init];
    [appDelegate loadEvents];
    
    if([self.tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
    {
        self.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }

}

- (void) editFilter:(id)sender
{
    // change filter here
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)removeOldObjectsFromDB
{
    NSDate *now = [[NSDate alloc] init];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *cmp = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:now];
    
    NSString *query = [NSString stringWithFormat:@"DELETE FROM event_items WHERE ndate < '%d-%02d-%02d 00:00:00';", [cmp year], [cmp month], [cmp day]];
    //NSString *query = [NSString stringWithFormat:@"DELETE FROM event_items WHERE nid = 124", [cmp year], [cmp month], [cmp day]];
    
    //NSLog(@"delete: %@", query);
    
    [manager executeQuery:query];
    
    
}

-(void)loadObjectsFromDB
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //NSLog(@"loading event data from db... with calendar %d", [appDelegate type]);
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM event_items WHERE calendar=%d OR calendar=2 OR calendar=%d ORDER BY ndate;", [appDelegate type],[appDelegate type]+3];
    
    NSArray * arr = [manager loadDataFromDB:query];
    
    if([arr count]>0)
    {
        //NSLog(@"found %d items in db", [arr count]);
        NSInteger indexOfId = [self->manager.arrColumnNames indexOfObject:@"nid"];
        NSInteger indexOfTitle = [self->manager.arrColumnNames indexOfObject:@"ntitle"];
        NSInteger indexOfText = [self->manager.arrColumnNames indexOfObject:@"ntext"];
        NSInteger indexOfDate = [self->manager.arrColumnNames indexOfObject:@"ndate"];
        NSInteger indexOfStatus = [self->manager.arrColumnNames indexOfObject:@"status"];
        NSInteger indexOfCalendar = [self->manager.arrColumnNames indexOfObject:@"calendar"];
        NSMutableArray * m_arr = [[NSMutableArray alloc] initWithCapacity:[arr count]];
        for(int i=0;i<[arr count];i++)
        {
            Item * it = [[Item alloc] init];
            [it setNtitle:[NSString stringWithFormat:@"%@", [[arr objectAtIndex:i] objectAtIndex:indexOfTitle]]];
            [it setNdate:[NSString stringWithFormat:@"%@", [[arr objectAtIndex:i] objectAtIndex:indexOfDate]]];
            [it setNtext:[NSString stringWithFormat:@"%@", [[arr objectAtIndex:i] objectAtIndex:indexOfText]]];
            [it setNid:[[NSString stringWithFormat:@"%@", [[arr objectAtIndex:i] objectAtIndex:indexOfId]] integerValue]];
            [it setCal:[[NSString stringWithFormat:@"%@", [[arr objectAtIndex:i] objectAtIndex:indexOfCalendar]] integerValue]];
            [it set_status:[[NSString stringWithFormat:@"%@", [[arr objectAtIndex:i] objectAtIndex:indexOfStatus]] integerValue]];
            [it setIsEvent:true];
            [m_arr addObject:it];
        }
        items = [[NSArray alloc] initWithArray:m_arr];
    }
    
    [self.tableView reloadData];
    
    // End the refreshing
    if (self.refreshControl) {
        
        /*NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd. MMM, HH:mm"];
        NSString *title = [NSString stringWithFormat:@"Letztes Update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;*/
        
        [self.refreshControl endRefreshing];
    }
}

-(void)updateTableRows
{
    [self removeOldObjectsFromDB];
    [self loadObjectsFromDB];
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    //self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    self.title = NSLocalizedString(@"Events & Training", @"Events & Training");
    //self.tabBarItem.image = [UIImage imageNamed:@"]

    return self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int color = [[items objectAtIndex:[indexPath row]] cal];
    switch (color) {
        case 0: // training men
            [cell setBackgroundColor:[UIColor colorWithRed:0.078f green:0.094f blue:0.34f alpha:0.1f]];
            break;
        case 1: // training women
            [cell setBackgroundColor:[UIColor colorWithRed:0.694f green:0.211f blue:0.373f alpha:0.1f]];
            break;
        case 2: // bern titans (all)
            [cell setBackgroundColor:[UIColor colorWithRed:0.745f green:0.427f blue:0.0f alpha:0.1f]];
            break;
        case 3: // nl men
            [cell setBackgroundColor:[UIColor colorWithRed:0.694f green:0.267f blue:0.055f alpha:0.1f]];
            break;
        case 4: // nl women
            [cell setBackgroundColor:[UIColor colorWithRed:0.44f green:0.086f blue:0.086f alpha:0.1f]];
            break;
        default:
            [cell setBackgroundColor:[UIColor colorWithRed:0 green:1.0f blue:0 alpha:0.1f]];
            break;
            
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    if([items count]>0)
    {
        [self.tableView setBackgroundView:bgView];
        [messageLabel setHidden:true];
        return [items count];
    } else {
        
        // Display a message when the table is empty
        messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"No data is currently available. Please pull down to refresh.";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.backgroundColor = [UIColor clearColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
        
        self.tableView.backgroundView = messageLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        return 0;
    }
}

-(void)setEvent:(int)event_id withCheck:(int)joined
{
    EventObject * eo = [[EventObject alloc] init];
    [eo setEid:event_id];
    [eo setJoin:YES];
    [changesWaitingForConfirmation addObject:eo];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
    [request setHTTPMethod:@"POST"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *postString = [NSString stringWithFormat:@"name=%@&pw=%@&type=updateevent&eid=%d&check=%d", [[appDelegate username] urlencode], [[appDelegate password] urlencode],event_id,joined];
    // NSLog(@"send token: %@", postString);
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection * conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No connection to server" message:@"Your changes could not be sent to the server. Please pull the list (refresh) to try again." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];

    [changesWaitingForConfirmation removeObjectAtIndex:0];
    [self updateTableRows];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(self->connData==nil)
        self->connData = [[NSMutableData alloc] init];
    [self->connData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:connData options:0 error:&localError];

    connData = [[NSMutableData alloc] init];
    
    NSString * eid = [parsedObject valueForKey:@"event"];
    NSString * status = [parsedObject valueForKey:@"status"];
    
    NSString * query = [NSString stringWithFormat:@"update event_items set status = %d where nid=%@", [status integerValue], eid];
    // Execute the query.
    [manager executeQuery:query];

    for(int i=0;i<[changesWaitingForConfirmation count];i++)
    {
        if([[changesWaitingForConfirmation objectAtIndex:i] eid]==[eid integerValue])
            [changesWaitingForConfirmation removeObjectAtIndex:i];
    }
    [self updateTableRows];
}

-(void) cellDidTap:(id) sender withCheck:(int)checked
{
    EventTableViewCell * cell = sender;
    //NSLog(@"call from %d", [[items objectAtIndex:[cell index]] nid]);
    [self setEvent:[[items objectAtIndex:[cell index]] nid] withCheck:checked];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * CellIdentifier = @"EventTableViewCell";
    EventTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil)
    {
        cell = [[EventTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.delegate = self;
    [cell setIndex:[indexPath row]];
    
    NSString * title = [NSString stringWithCString:[[[items objectAtIndex:[indexPath row]] ntitle] cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
    
    // extract date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *myDate =[dateFormat dateFromString:[[items objectAtIndex:[indexPath row]] ndate]];
    
    // get week number of year
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *dateComponent = [calender components:(NSCalendarUnitWeekOfYear) fromDate:myDate];
    title = [title stringByAppendingString:[NSString stringWithFormat:@" - KW%02d", [dateComponent weekOfYear]]];
    [cell.title setText:title];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEEE dd.MM.yyyy HH:mm"];
    [cell.subTitle setText:[formatter stringFromDate:myDate]];
    [cell setStatus:[[items objectAtIndex:[indexPath row]] _status]];
    if([[[items objectAtIndex:[indexPath row]] ntext] length]>0)
    {
        [cell setHasDetail:true];
    }
    else
        [cell setHasDetail:false];
    [cell refresh];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[[items objectAtIndex:[indexPath row]] ntext] length]>0)
    {
        DetailViewController * dvc = [[DetailViewController alloc] init];
        [dvc setItem:[items objectAtIndex:[indexPath row]]];
        dvc.hidesBottomBarWhenPushed = YES;
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self.navigationController pushViewController:dvc animated:YES];
    }
    else
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kCellHeight * 2.0;
}

@end
