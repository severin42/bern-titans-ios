//
//  DetailViewController.h
//  titans
//
//  Created by Severin Zumbrunn on 04.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"
#import "User.h"
#import "AppDelegate.h"
#import "UserTableViewController.h"

#define APP_URL @"http://titansapp.bernlacrosse.ch/app.php"

@interface DetailViewController : UIViewController <UIWebViewDelegate,NSURLConnectionDelegate>
{
    UIWebView * webView;
    NSMutableData * connData;
    UserTableViewController * utvc;
    UILabel * users_enrolled_view;
}

@property(nonatomic,retain) Item * item;

@end
