//
//  NSString+MD5.h
//  titans
//
//  Created by Severin Zumbrunn on 05.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(MD5)

- (NSString *)urlencode;
-(NSString *)htmlDecode;

@end