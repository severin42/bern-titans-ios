//
//  NewsTableViewCell.m
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "NewsTableViewCell.h"

@implementation NewsTableViewCell

@synthesize title,subTitle, hasDetail;

- (void)awakeFromNib {
    
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        CGSize screen = [[UIScreen mainScreen] bounds].size;
        [self setBackgroundColor:[UIColor clearColor]];
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, screen.width-50, 40)];

        [self.title setTextColor:[UIColor blackColor]];
        [self.title setFont:[UIFont fontWithName:@"Arial" size:18.0f]];
        self.title.numberOfLines = 1;
        self.title.minimumScaleFactor = 0.7;
        self.title.adjustsFontSizeToFitWidth = YES;
        [self addSubview:self.title];

        self.subTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 25, screen.width-50, 30)];
        [self.subTitle setTextColor:[UIColor blackColor]];
        [self.subTitle setTextAlignment:NSTextAlignmentLeft];
        [self.subTitle setFont:[UIFont fontWithName:@"Arial" size:14.0f]];
        [self addSubview:self.subTitle];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)refresh
{
    if(hasDetail)
    {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

@end
