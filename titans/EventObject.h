//
//  EventObject.h
//  titans
//
//  Created by Severin Zumbrunn on 05.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventObject : NSObject

@property int eid;
@property BOOL join;

@end
