//
//  RegisterViewController.h
//  titans
//
//  Created by Severin Zumbrunn on 08.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate,NSURLConnectionDelegate,UITextViewDelegate>
{
    UITextField * fUserName;
    UITextField * fName;
    UITextField * fPassword;
    UITextField * fPasswordRepeat;
    UITextField * fEmail;
    UITextField * fEmailRepeat;
    
    UIView * containerView;
    NSMutableData * connData;
    NSURLConnection * conn;

    UITextView * fView;
}

-(void) cancel:(id)sender;
-(void) regist:(id)sender;
-(void)onKeyboardShow:(NSNotification *)notification;
-(void)onKeyboardHide:(NSNotification *)notification;

@end
