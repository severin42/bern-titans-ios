//
//  DetailViewController.m
//  titans
//
//  Created by Severin Zumbrunn on 04.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "DetailViewController.h"
#import "NSString+MD5.h"

#define MIN_VALID_URL_LENGTH 6

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize item;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    CGSize screen = [[UIScreen mainScreen] bounds].size;

    
    NSString *cssPath = [[NSBundle mainBundle] pathForResource:@"style" ofType:@"css"];
    NSString * base_url = @"http://www.bernlacrosse.ch";
    
    NSString * content = [NSString stringWithFormat:@"<html><head><meta charset='utf-8'><link rel='stylesheet' href='%@' type='text/css'/></head><body>", cssPath];
    NSString * title;
    
    if([self.item isEvent])
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *myDate =[dateFormat dateFromString:[item ndate]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE dd.MM.yyyy HH:mm"];
        content = [content stringByAppendingString:[NSString stringWithFormat:@"Datum: %@<br/>", [formatter stringFromDate:myDate]]];
        
        title = [NSString stringWithCString:[[item ntitle] cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
        //title =[item ntitle];
        
        NSCalendar *calender = [NSCalendar currentCalendar];
        NSDateComponents *dateComponent = [calender components:(NSCalendarUnitWeekOfYear) fromDate:myDate];
        title = [title stringByAppendingString:[NSString stringWithFormat:@" - KW%02d", [dateComponent weekOfYear]]];
    }
    else
    {
        title = [item ntitle];
    }
    
    content = [content stringByAppendingString:[[item ntext] htmlDecode]];

    content = [content stringByAppendingString:@"</body></html>"];
    content = [content stringByReplacingOccurrencesOfString:@"src=\"" withString:[NSString stringWithFormat:@"src=\"%@/",base_url]];
    
    //if([self.item isEvent])
        content = [NSString stringWithCString:[content cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];

    UIImageView * imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.jpg"]];
    [imgView setFrame:CGRectMake(0, 60, screen.width, screen.height-120)];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    [imgView setAlpha:0.05f];
    
    [self.view addSubview:imgView];
    
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64, screen.width, screen.height-70)];
    webView.opaque = NO;
    webView.backgroundColor = [UIColor clearColor];
    
    [webView loadHTMLString:content baseURL:[NSURL URLWithString:@""]];
    [webView setDelegate:self];
    [self.view addSubview:webView];
    
    [self setTitle:title];
    
    if([self.item isEvent])
    {
        // Here is the destinction made between an event and a news item
        utvc = [[UserTableViewController alloc] initWithStyle:UITableViewStylePlain];
        [utvc.view setFrame:CGRectMake(0, screen.height/2, screen.width, screen.height/2)];
        [self.view addSubview:utvc.view];
        [self addChildViewController:utvc];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
        [request setHTTPMethod:@"POST"];
        NSString *postString = [NSString stringWithFormat:@"name=%@&pw=%@&type=geteventdetails&eid=%d", [[appDelegate username] urlencode], [[appDelegate password] urlencode],[self.item nid]];
        // NSLog(@"send token: %@", postString);
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSURLConnection *connection= [[NSURLConnection alloc] initWithRequest:request delegate:self];
        NSLog(@"sending token...");
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        users_enrolled_view = [[UILabel alloc] initWithFrame:CGRectMake(15, screen.height/2-30, screen.width-30, 30)];
        [users_enrolled_view setText:@"Angemeldet: 0"];
        [self.view addSubview:users_enrolled_view];
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(self->connData==nil)
        self->connData = [[NSMutableData alloc] init];
    [self->connData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSError *localError = nil;
    
    NSString * str = [[NSString alloc] initWithData:connData encoding:NSUTF8StringEncoding];
    str = [str stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    connData = [[NSMutableData alloc] init];
    
    //NSLog(@"%@",str);
    
    int amount_user_enrolled = 0;
    
    NSArray *results = [parsedObject valueForKey:@"evusers"];
    NSMutableArray * users = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in results) {
        User * user = [[User alloc] init];
        [user setName:[dic objectForKey:@"user"]];
        [user set_status:[[dic objectForKey:@"status"] integerValue]];
        [user setUid:[[dic objectForKey:@"id"] integerValue]];
        [users addObject:user];
        if([user _status]==0)
            amount_user_enrolled++;
    }
    //NSLog(@"array content count: %d", [users count]);
    
    // sort users
    NSArray *sortedUsers = [users sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        User * u1 = obj1;
        User * u2 = obj2;
        
        if (u1._status < u2._status) {
            return NSOrderedAscending;
        } else if (u1._status > u2._status) {
            return NSOrderedDescending;
        } else {
            return [u1.name compare:u2.name];
        }
        
    }];
    
    [utvc updateTableWithArray:sortedUsers];

    [users_enrolled_view setText:[NSString stringWithFormat:@"Angemeldet: %d", amount_user_enrolled]];
    
}

-(BOOL)webView:(UIWebView *)wView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if([[[request URL] absoluteString] rangeOfString:@"file:///"].location != NSNotFound)
    {
        
        NSString * url = [NSString stringWithFormat:@"%@",[request URL]];
        url = [url substringFromIndex:7];
        
        if([url length]>=MIN_VALID_URL_LENGTH)
        {
            NSString * base_url = @"http://www.bernlacrosse.ch";
            request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",base_url,url]]];
            [wView loadRequest:request];
            //NSLog(@"%@",[[request URL] absoluteString]);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        }
    }
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
