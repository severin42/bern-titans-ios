//
//  UserTableViewCell.h
//  titans
//
//  Created by Severin Zumbrunn on 03.05.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserTableViewCell : UITableViewCell

@property(nonatomic,retain) UILabel * name;
@property(nonatomic,retain) UISegmentedControl * status;

-(void)setStatusForValue:(int)val;

@end
