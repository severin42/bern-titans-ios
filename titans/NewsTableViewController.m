//
//  NewsTableViewController.m
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "NewsTableViewController.h"
#import "NewsTableViewCell.h"
#import "DetailViewController.h"
#import "AppDelegate.h"

@interface NewsTableViewController ()

@end

@implementation NewsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    manager = [[DBManager alloc] initWithDatabaseFilename:@"titansdb.sql"];
    
    [self loadObjectsFromDB];
 
    CGSize screen = [[UIScreen mainScreen] bounds].size;
    bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.jpg"]];
    [bgView setFrame:CGRectMake(0, 60, screen.width*0.9, screen.height-120)];
    [bgView setContentMode:UIViewContentModeScaleAspectFit];
    [bgView setAlpha:0.05f];
    [self.tableView setBackgroundView:bgView];
    

    self.navigationController.navigationBar.tintColor = [UIColor orangeColor];
    self.navigationController.navigationBar.translucent = false;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor blackColor];
    [self.refreshControl addTarget:appDelegate
                            action:@selector(fetchNewData)
                  forControlEvents:UIControlEventValueChanged];
    [self.refreshControl endRefreshing];
    
    if([self.tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
    {
        self.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
}

-(void)loadObjectsFromDB
{
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM news_items ORDER BY ndate DESC;"];
    
    NSArray * arr = [manager loadDataFromDB:query];
    
    if([arr count]>0)
    {
        NSInteger indexOfId = [self->manager.arrColumnNames indexOfObject:@"nid"];
        NSInteger indexOfTitle = [self->manager.arrColumnNames indexOfObject:@"ntitle"];
        NSInteger indexOfText = [self->manager.arrColumnNames indexOfObject:@"ntext"];
        NSInteger indexOfDate = [self->manager.arrColumnNames indexOfObject:@"ndate"];
        NSMutableArray * m_arr = [[NSMutableArray alloc] initWithCapacity:[arr count]];
        for(int i=0;i<[arr count];i++)
        {
            Item * it = [[Item alloc] init];
            [it setNtitle:[NSString stringWithFormat:@"%@", [[arr objectAtIndex:i] objectAtIndex:indexOfTitle]]];
            [it setNdate:[NSString stringWithFormat:@"%@", [[arr objectAtIndex:i] objectAtIndex:indexOfDate]]];
            [it setNtext:[NSString stringWithFormat:@"%@", [[arr objectAtIndex:i] objectAtIndex:indexOfText]]];
            [it setNid:[[NSString stringWithFormat:@"%@", [[arr objectAtIndex:i] objectAtIndex:indexOfId]] integerValue]];
            [m_arr addObject:it];
        }
        items = [[NSArray alloc] initWithArray:m_arr];
    }
    
    [self.tableView reloadData];
    
    // End the refreshing
    if (self.refreshControl) {
        
        [self.refreshControl endRefreshing];
    }
}

-(void)updateTableRows
{
    [self loadObjectsFromDB];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    //self = [super initWithNibName:@"NewsTableViewController" bundle:nibBundleOrNil];
    self.title = NSLocalizedString(@"News", @"News");
    //self.tabBarItem.image = [UIImage imageNamed:@"]
    
    return self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if([items count]>0)
    {
        [self.tableView setBackgroundView:bgView];
        [messageLabel setHidden:true];
        return [items count];
    } else {
    
        // Display a message when the table is empty
        messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        [messageLabel setBackgroundColor:[UIColor clearColor]];
        messageLabel.text = @"No data is currently available. Please pull down to refresh.";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
    
        self.tableView.backgroundView = messageLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * CellIdentifier = @"NewsTableViewCell";
    NewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil)
    {
        cell = [[NewsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [cell.title setText:[[items objectAtIndex:[indexPath row]] ntitle]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *myDate =[dateFormat dateFromString:[[items objectAtIndex:[indexPath row]] ndate]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd.MM.yyyy HH:mm"];
    [cell.subTitle setText:[formatter stringFromDate:myDate]];
    if([[[items objectAtIndex:[indexPath row]] ntext] length]>0)
        [cell setHasDetail:true];
    else
        [cell setHasDetail:false];
    [cell refresh];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[[items objectAtIndex:[indexPath row]] ntext] length]>0)
    {
        DetailViewController * dvc = [[DetailViewController alloc] init];
        [dvc setItem:[items objectAtIndex:[indexPath row]]];
        dvc.hidesBottomBarWhenPushed = YES;
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self.navigationController pushViewController:dvc animated:YES];
    }
    else
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kCellHeight * 1.2;
}

@end
