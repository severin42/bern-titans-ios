//
//  NewsTableViewController.h
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

#define kCellHeight 44

@protocol PushNotifications <NSObject>

@optional
-(void) updateTableRows;

@end

@interface NewsTableViewController : UITableViewController <UITableViewDelegate,PushNotifications>
{
    NSArray * items;
    DBManager * manager;
    UILabel *messageLabel;
    UIImageView * bgView;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
-(void)loadObjectsFromDB;


@end
