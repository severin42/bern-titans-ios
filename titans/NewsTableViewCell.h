//
//  NewsTableViewCell.h
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableViewCell : UITableViewCell

@property (nonatomic,retain) UILabel * title;
@property (nonatomic,retain) UILabel * subTitle;
@property BOOL hasDetail;

-(void) refresh;

@end
