//
//  AppDelegate.h
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ViewController.h"
#import "DBManager.h"
#import "NSString+MD5.h"

#define APP_URL @"http://titansapp.bernlacrosse.ch/app.php"

@interface AppDelegate : UIResponder <UIApplicationDelegate, NSURLConnectionDelegate,NSURLConnectionDataDelegate>
{
    BOOL registered;
    DBManager * manager;
    NSMutableData * connData;
    NSURLConnection * conn;
    BOOL hasNewData;
    BOOL loginScreenShown;
    UIApplication * app;
}
@property (strong,nonatomic) NSString * username;
@property (strong,nonatomic) NSString * password;
@property int type;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;
@property (strong, nonatomic) UINavigationController *nav;

-(void) sendProviderDeviceToken:(NSString *)devTokenBytes;
-(void)registerForNotifications;
-(void) fetchNewData;
-(void) loadEvents;
-(void) sendPositionWithLatitude:(float)latitude andLongitude:(float)longitude;
-(void) setCredentialsWithUsername:(NSString *)username andPassword:(NSString *)password andType:(int)type;

@end

