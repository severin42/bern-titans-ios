//
//  AnnotationObject.h
//  titans
//
//  Created by Severin Zumbrunn on 26.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface AnnotationObject : NSObject

@property int subject;
@property int timestamp;
@property CGPoint point;
@property int radius;

@end
