//
//  User.h
//  titans
//
//  Created by Severin Zumbrunn on 03.05.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property(nonatomic,retain) NSString * name;
@property int uid;
@property int _status;

@end
