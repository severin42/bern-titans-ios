//
//  MapAnnotationViewController.h
//  titans
//
//  Created by Severin Zumbrunn on 26.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AnnotationObject.h"
#import "WallBallViewController.h"

@class WallBallViewController;

@interface MapAnnotationViewController : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate>
{
    NSArray * pickerContent;

}

@property(nonatomic,retain) UIPickerView * timePicker;
@property(nonatomic,retain) UISegmentedControl *subject;
@property(nonatomic,retain) UISlider *radius;
@property(nonatomic,retain) UILabel *radiusNumeral;
@property(nonatomic,retain) UIButton *sendButton;
@property CGPoint point;
@property(nonatomic,retain) WallBallViewController * master;

@end
