//
//  MapAnnotationViewController.m
//  titans
//
//  Created by Severin Zumbrunn on 26.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "MapAnnotationViewController.h"

@interface MapAnnotationViewController ()

@end

@implementation MapAnnotationViewController

@synthesize timePicker,subject,point,master,radius,radiusNumeral,sendButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    CGSize screen = [[UIScreen mainScreen] bounds].size;
    UIImageView * imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.jpg"]];
    [imgView setFrame:CGRectMake(0, 20, screen.width, screen.height-120)];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    [imgView setAlpha:0.2f];
    
    //NSLog(@"screen size: %f,%f",screen.height,screen.width);
    
    [self.view addSubview:imgView];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self setTitle:@"Share an Event"];
    UILabel * timelabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 10*screen.height/480, screen.width-40,30)];
    [timelabel setText:@"When?"];
    [self.view addSubview:timelabel];
    UILabel * subjectlabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 150*screen.height/480, screen.width-40,30)];
    [subjectlabel setText:@"What?"];
    [self.view addSubview:subjectlabel];
    timePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(20, screen.height/480, screen.width-40, 120)];
    pickerContent = [[NSArray alloc] initWithObjects:@"5 minutes",
                              @"10 minutes",
                              @"15 minutes",
                              @"20 minutes",
                              @"25 minutes",
                              @"30 minutes",
                              @"45 minutes",
                              @"1 hour",nil];
    [timePicker setDataSource:self];
    [timePicker setDelegate:self];
    [self.view addSubview:timePicker];
    
    subject = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Wallball",@"Adhoc Training",@"Eis näh",nil]];
    [subject setFrame:CGRectMake(20, 190*screen.height/480, screen.width-40, 30)];
    [self.view addSubview:subject];
    
    UILabel * radiusLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 230*screen.height/480, screen.width-40,30)];
    [radiusLabel setText:@"Radius to advertise"];
    [self.view addSubview:radiusLabel];
    
    radius = [[UISlider alloc] initWithFrame:CGRectMake(20,270*screen.height/480, screen.width-120, 30)];
    [self.view addSubview:radius];
    [radius setValue:0.5f animated:false];
    [radius addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    radiusNumeral = [[UILabel alloc] initWithFrame:CGRectMake(screen.width-80,270*screen.height/480, screen.width-40, 30)];
    [radiusNumeral setText:@"5km"];
    [self.view addSubview:radiusNumeral];
    
    sendButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 320*screen.height/480, screen.width-40, 30)];
    [sendButton setTitle:@"Tell your team!" forState:UIControlStateNormal];
    [sendButton addTarget:self action:@selector(submit:) forControlEvents:UIControlEventTouchUpInside];
    [sendButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self.view addSubview:sendButton];
    // Do any additional setup after loading the view.
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    float val = [sender value];
    [radiusNumeral setText:[NSString stringWithFormat:@"%d km",(int)roundf(val*10)]];
}

-(IBAction)submit:(id)sender
{
    AnnotationObject * obj = [[AnnotationObject alloc] init];
    [obj setPoint:point];
    [obj setSubject:(int)[subject selectedSegmentIndex]];
    [obj setTimestamp:[[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970]];
    int row = (int)[timePicker selectedRowInComponent:0];
    if(row<=5)
    {
        [obj setTimestamp:[obj timestamp]+60*5*row];
    }
    else if(row==6)
    {
        [obj setTimestamp:[obj timestamp]+60*45];
    }
    else
    {
        [obj setTimestamp:[obj timestamp]+60*60];
    }
    [obj setRadius:(int)roundf([radius value]*10)];
    [master putNewAnnotation:obj];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [pickerContent objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

-(int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [pickerContent count];
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    //self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    self.title = NSLocalizedString(@"mavc", @"mavc");
    //self.tabBarItem.image = [UIImage imageNamed:@"]
    
    return self;
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
    }
    [super viewWillDisappear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
