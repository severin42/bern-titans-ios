//
//  RegisterViewController.m
//  titans
//
//  Created by Severin Zumbrunn on 08.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "RegisterViewController.h"
#import "AppDelegate.h"

#define ALPHA                   @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define NUMERIC                 @"1234567890"
#define ALPHA_NUMERIC           ALPHA NUMERIC

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    connData = nil;
    // Do any additional setup after loading the view.
    CGSize screen = [[UIScreen mainScreen] bounds].size;
    
    int height_pos = screen.height/2 - 240;
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen.width, screen.height)];
    [containerView setBackgroundColor:[UIColor clearColor]];

    
    UILabel * lName = [[UILabel alloc] initWithFrame:CGRectMake(20, 40+height_pos, 200, 30)];
    [lName setText:@"Full name:"];
    [lName setTag:1];
    fName = [[UITextField alloc] initWithFrame:CGRectMake(20, 70+height_pos, screen.width-40, 30)];
    [fName setTag:1];
    fName.borderStyle = UITextBorderStyleRoundedRect;
    [fName setKeyboardType:UIKeyboardTypeAlphabet];
    [fName setReturnKeyType:UIReturnKeyNext];
    [fName setDelegate:self];
    
    UILabel * lUserName = [[UILabel alloc] initWithFrame:CGRectMake(20, 100+height_pos, 200, 30)];
    [lUserName setText:@"Username:"];
    [lUserName setTag:1];
    fUserName = [[UITextField alloc] initWithFrame:CGRectMake(20, 130+height_pos, screen.width-40, 30)];
    [fUserName setTag:2];
    fUserName.borderStyle = UITextBorderStyleRoundedRect;
    [fUserName setKeyboardType:UIKeyboardTypeAlphabet];
    [fUserName setReturnKeyType:UIReturnKeyNext];
    [fUserName setDelegate:self];
    
    UILabel * lPassword = [[UILabel alloc] initWithFrame:CGRectMake(20, 160+height_pos, 200, 30)];
    [lPassword setText:@"Password:"];
    [lPassword setTag:1];
    fPassword = [[UITextField alloc] initWithFrame:CGRectMake(20, 190+height_pos, screen.width-40, 30)];
    [fPassword setTag:3];
    fPassword.borderStyle = UITextBorderStyleRoundedRect;
    [fPassword setKeyboardType:UIKeyboardTypeAlphabet];
    [fPassword setSecureTextEntry:YES];
    [fPassword setReturnKeyType:UIReturnKeyNext];
    [fPassword setDelegate:self];
    
    UILabel * lPasswordRepeat = [[UILabel alloc] initWithFrame:CGRectMake(20, 220+height_pos, 200, 30)];
    [lPasswordRepeat setText:@"Repeat password:"];
    [lPasswordRepeat setTag:1];
    fPasswordRepeat = [[UITextField alloc] initWithFrame:CGRectMake(20, 250+height_pos, screen.width-40, 30)];
    [fPasswordRepeat setTag:4];
    fPasswordRepeat.borderStyle = UITextBorderStyleRoundedRect;
    [fPasswordRepeat setKeyboardType:UIKeyboardTypeAlphabet];
    [fPasswordRepeat setSecureTextEntry:YES];
    [fPasswordRepeat setReturnKeyType:UIReturnKeyNext];
    [fPasswordRepeat setDelegate:self];
    
    UILabel * lMail = [[UILabel alloc] initWithFrame:CGRectMake(20, 280+height_pos, 200, 30)];
    [lMail setText:@"Email:"];
    [lMail setTag:1];
    fEmail = [[UITextField alloc] initWithFrame:CGRectMake(20, 310+height_pos, screen.width-40, 30)];
    [fEmail setTag:5];
    fEmail.borderStyle = UITextBorderStyleRoundedRect;
    [fEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    [fEmail setReturnKeyType:UIReturnKeyNext];
    [fEmail setDelegate:self];
    
    
    UILabel * lMailRepeat = [[UILabel alloc] initWithFrame:CGRectMake(20, 340+height_pos, 200, 30)];
    [lMailRepeat setText:@"Repeat email:"];
    [lMailRepeat setTag:1];
    fEmailRepeat = [[UITextField alloc] initWithFrame:CGRectMake(20, 370+height_pos, screen.width-40, 30)];
    [fEmailRepeat setTag:6];
    fEmailRepeat.borderStyle = UITextBorderStyleRoundedRect;
    [fEmailRepeat setKeyboardType:UIKeyboardTypeEmailAddress];
    [fEmailRepeat setReturnKeyType:UIReturnKeySend];
    [fEmailRepeat setDelegate:self];
    
    fView = [[UITextView alloc] initWithFrame:CGRectMake(20, 280+height_pos, screen.width-40, 120)];
    [fView setDataDetectorTypes:UIDataDetectorTypeAll];
    
    [fView setText:@" "];
    [fView setEditable:NO];
    [fView setUserInteractionEnabled:YES];
    [fView setDelegate:self];
    [fView setBackgroundColor:[UIColor clearColor]];
    [fView setFont:[UIFont fontWithName:@"Helvetica" size:16.0f]];
    [fView setHidden:true];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setFrame:CGRectMake(20, 400+height_pos, 60, 40)];
    [button setTag:1];
    [button setTitle:@"Register" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(regist:) forControlEvents:UIControlEventTouchUpInside];
    UIButton * cancel = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cancel setFrame:CGRectMake(screen.width-20-60, 400+height_pos, 60, 40)];
    [cancel setTag:1];
    [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView * imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.jpg"]];
    [imgView setFrame:CGRectMake(0, 60, screen.width, screen.height-120)];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    [imgView setAlpha:0.05f];
    [self.view addSubview:imgView];
    [containerView addSubview:lName];
    [containerView addSubview:lMailRepeat];
    [containerView addSubview:lMail];
    [containerView addSubview:lUserName];
    [containerView addSubview:lPassword];
    [containerView addSubview:lPasswordRepeat];
    [containerView addSubview:button];
    [containerView addSubview:cancel];
    [containerView addSubview:fName];
    [containerView addSubview:fUserName];
    [containerView addSubview:fPassword];
    [containerView addSubview:fPasswordRepeat];
    [containerView addSubview:fEmail];
    [containerView addSubview:fEmailRepeat];
    [containerView addSubview:fView];
    [self.view addSubview:containerView];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)onKeyboardShow:(NSNotification *)notification
{
    /*CGSize screen = [[UIScreen mainScreen] bounds].size;
    CGRect frame = CGRectMake(0, -screen.height/3+45, screen.width, screen.height);
    [UIView animateWithDuration:0.5
                          delay:0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         containerView.frame = frame;
                         
                     }
                     completion:^(BOOL finished){
                     }];*/
}

-(void)onKeyboardHide:(NSNotification *)notification
{
    CGSize screen = [[UIScreen mainScreen] bounds].size;
    CGRect frame = CGRectMake(0, 0, screen.width, screen.height);
    [UIView animateWithDuration:0.5
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         containerView.frame = frame;
                     }
                     completion:^(BOOL finished){
                     }];
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if (sender.frame.origin.y > 120)
    {
        int height = sender.frame.origin.y-100;
        
        //NSLog(@"active height: %f",sender.frame.origin.y);
        //move the main view, so that the keyboard does not hide it.
            CGSize screen = [[UIScreen mainScreen] bounds].size;
            CGRect frame = CGRectMake(0, -height, screen.width, screen.height);
            [UIView animateWithDuration:0.5
                                  delay:0
                                options: UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 containerView.frame = frame;
                                 
                             }
                             completion:^(BOOL finished){
                             }];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
        [self regist:textField];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

-(void) cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) regist:(id)sender
{
    connData = nil;
    [fView setHidden:true];
    [self.view endEditing:YES];
    bool isOkay[6] = {false,false,false,false,false,false};
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; //  return 0;
    if([emailTest evaluateWithObject:fEmail.text])
    {
        [fEmail setBackgroundColor:[UIColor greenColor]];
        isOkay[4] = true;
        if([fEmailRepeat.text isEqualToString:fEmail.text])
        {
            [fEmailRepeat setBackgroundColor:[UIColor greenColor]];
            isOkay[5] = true;
        }
        else
        {
            [fEmailRepeat setBackgroundColor:[UIColor redColor]];
        }
    }
    else
    {
        [fEmail setBackgroundColor:[UIColor redColor]];
    }
    
    if([fName.text length] > 3)
    {
        [fName setBackgroundColor:[UIColor greenColor]];
        isOkay[0] = true;
    }
    else
    {
        [fName setBackgroundColor:[UIColor redColor]];
    }
    
    if([fUserName.text length] > 3)
    {
        [fUserName setBackgroundColor:[UIColor greenColor]];
        isOkay[1] = true;
    }
    else
    {
        [fUserName setBackgroundColor:[UIColor redColor]];
    }
    
    if([fPassword.text length] > 4)
    {
        [fPassword setBackgroundColor:[UIColor greenColor]];
        isOkay[2] = true;
        if([fPasswordRepeat.text isEqualToString:fPassword.text])
        {
            [fPasswordRepeat setBackgroundColor:[UIColor greenColor]];
            isOkay[3] = true;
        }
        else
        {
            [fPasswordRepeat setBackgroundColor:[UIColor redColor]];
        }
    }
    else
    {
        [fPassword setBackgroundColor:[UIColor redColor]];
    }

    if(isOkay[0] && isOkay[1] && isOkay[2] && isOkay[3] && isOkay[4] && isOkay[5])
    {
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
        [request setHTTPMethod:@"POST"];
        NSLog(@"form check is okay %@", [fEmail text]);
        
        NSString *postString = [NSString stringWithFormat:@"name=%@&fullname=%@&email=%@&pw=%@&type=registeruser", [fUserName.text urlencode], [fName.text urlencode], [fEmail.text urlencode], [fPassword.text urlencode]];
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection error!");
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection Error" message:@"Please check your internet connection and try again." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(self->connData==nil)
        self->connData = [[NSMutableData alloc] init];
    [self->connData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:connData options:0 error:&localError];
    int registered = [[parsedObject valueForKey:@"registered"] integerValue];
    NSLog(@"Register: %@", [[NSString alloc] initWithData:connData encoding:NSUTF8StringEncoding]);
    
    if(registered)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Registration successful" message:@"Thank you for registering. Before you log in, please check your email to activate your account." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Registration failed" message:[parsedObject valueForKey:@"msg"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    //self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    self.title = NSLocalizedString(@"Login", @"Login");
    //self.tabBarItem.image = [UIImage imageNamed:@"]
    
    return self;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
