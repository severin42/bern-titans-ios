//
//  WallBallViewController.m
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "WallBallViewController.h"


@interface WallBallViewController ()

@end

@implementation WallBallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    gmap = [[MKMapView alloc] initWithFrame:self.view.frame];
    
    gmap.delegate = self;
    [self.view addSubview:gmap];
    
    locationManager = [[CLLocationManager alloc] init];
    SEL requestSelector = NSSelectorFromString(@"requestWhenInUseAuthorization");
    if ([locationManager respondsToSelector:requestSelector]) {
        [locationManager performSelector:requestSelector withObject:NULL];
    }
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    [self getLocationData];
    NSTimer * timer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(passPosition:) userInfo:nil repeats:YES];
    
    UILongPressGestureRecognizer *lpgr
    = [[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = .5; //seconds
    lpgr.delegate = self;
    [self->gmap addGestureRecognizer:lpgr];
    // Do any additional setup after loading the view.
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
    if ([[annotation title] isEqualToString:@"Current Location"]) {
        return nil;
    }
    
    UserAnnotation * ann = (UserAnnotation *) annotation;
    
    MKAnnotationView *annView = [[MKAnnotationView alloc ] initWithAnnotation:ann reuseIdentifier:@"currentloc"];
    if ([ann category]==0)
        annView.image = [ UIImage imageNamed:@"brickwall.png" ];
    else if ([ann category]==1)
        annView.image = [ UIImage imageNamed:@"lacrosse1-2.png" ];
    else if([ann category]==2)
        annView.image = [ UIImage imageNamed:@"beer-icon.png" ];
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [infoButton addTarget:self action:@selector(showDetailsView)
         forControlEvents:UIControlEventTouchUpInside];
    //annView.rightCalloutAccessoryView = infoButton;
    annView.canShowCallout = YES;
    return annView;
}

-(void)passPosition:(NSTimer *)timer
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
    if(latitude!=0 && latitude != 0)
        [appDelegate sendPositionWithLatitude:latitude andLongitude:longitude];
    [self getLocationData];
}

- (IBAction)getCurrentLocation:(id)sender {
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    /*UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];*/
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        [gmap setShowsUserLocation:YES];
        longitude = currentLocation.coordinate.longitude;
        latitude = currentLocation.coordinate.latitude;
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 800, 800);
        [gmap setRegion:[gmap regionThatFits:region] animated:YES];
    }
}

-(void)updateTableRows
{
    // global update function called from Appdelegate
    [self getLocationData];
    NSLog(@"reload locations");
}

-(void)getLocationData
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"name=%@&pw=%@&type=getcurrentevents", [[appDelegate username] urlencode], [[appDelegate password] urlencode]];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    self->conn= [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    NSLog(@"fetching data....");
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded) {
        
    }
    else if (sender.state == UIGestureRecognizerStateBegan){
        // Player set a new position
        CGPoint p = [sender locationInView:self->gmap];
        CLLocationCoordinate2D tapPoint = [self->gmap convertPoint:p toCoordinateFromView:self.view];
        
        CGPoint point = CGPointMake(tapPoint.latitude,tapPoint.longitude);
        
        MapAnnotationViewController * mavc = [[MapAnnotationViewController alloc] initWithNibName:@"MapAnnotationViewController" bundle:nil];
        [mavc setPoint:point];
        [mavc setMaster:self];
        [self.navigationController pushViewController:mavc animated:YES];
    }

}

-(void) putNewAnnotation:(AnnotationObject *)obj
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
   
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"name=%@&pw=%@&type=addcurrentevent&lon=%f&lat=%f&cat=%d&time=%d&rad=%d", [[appDelegate username] urlencode],
                                                                                                [[appDelegate password] urlencode],
                                                                                                obj.point.y,
                                                                                                obj.point.x,
                                                                                                [obj subject],
                                                                                                [obj timestamp],
                                                                                                [obj radius]];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    self->conn= [[NSURLConnection alloc] initWithRequest:request delegate:self];
    

}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"errror!");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(self->connData==nil)
        self->connData = [[NSMutableData alloc] init];
    [self->connData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *localError = nil;
    
    NSString *str = [[NSString alloc] initWithData:connData encoding:NSISOLatin1StringEncoding];
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&localError];
    NSLog(@"str: %@",str);
    if([parsedObject count]==0)
    {
            [self getLocationData];
    }
    NSLog(@"locations: %@",parsedObject);
    connData = [[NSMutableData alloc] init];
    
    NSArray *results = [parsedObject valueForKey:@"curr_events"];
    if([results count]>0)
        [gmap removeAnnotations:[gmap annotations]];
    for(NSDictionary * dic in results)
    {
        UserAnnotation *ann = [[UserAnnotation alloc] init];

        NSString * people = [NSString stringWithString:[dic objectForKey:@"people"]];
        
        NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate * date = [formatter dateFromString:[dic objectForKey:@"takeplace"]];
        NSDate * now = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
            
        NSTimeInterval diff = [date timeIntervalSinceDate:now];
        switch ([[dic objectForKey:@"category"] integerValue]) {
            case 0:
                ann.title = [NSString stringWithFormat:@"Wallball"];
                break;
            case 1:
                ann.title = [NSString stringWithFormat:@"AdHoc Training"];
                break;
            case 2:
                ann.title = [NSString stringWithFormat:@"Eis näh"];
                break;
                    
            default:
                break;
        }
    
            
        NSString * time;
        if ([now compare:date] == NSOrderedAscending) {
            int min = round(diff/60);
            time = [NSString stringWithFormat:@"in %d min",min];
        }
        else
        {
            time = @"jetzt";
        }
        ann.category = [[dic objectForKey:@"category"] integerValue];
        ann.subtitle=[NSString stringWithFormat:@"%@ mit %@",time, people];
        ann.coordinate = CLLocationCoordinate2DMake([[dic objectForKey:@"lat"] doubleValue], [[dic objectForKey:@"lon"] doubleValue]);
        [gmap addAnnotation:ann];
    }
}

- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

/*- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [gmap setRegion:[gmap regionThatFits:region] animated:YES];
}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    //self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    self.title = NSLocalizedString(@"Network", @"Network");
    //self.tabBarItem.image = [UIImage imageNamed:@"]
    return self;
}

@end
