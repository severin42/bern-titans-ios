//
//  AppDelegate.m
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "NewsTableViewController.h"
#import "EventTableViewController.h"
#import "Item.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize username, password,type;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    if([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge];
    }

    loginScreenShown = false;
    NSLog(@"launched");
    self->manager = [[DBManager alloc] initWithDatabaseFilename:@"titansdb.sql"];
    
    NSString *query = [NSString stringWithFormat:@"SELECT name FROM sqlite_master WHERE type='table';"];
    
    NSArray * arr = [manager loadDataFromDB:query];
    
    //  [manager executeQuery:@"drop table news_items;"];
    
    if([arr count]<3)
    {
       /* UIAlertView *errorAlert = [[UIAlertView alloc]
                                   initWithTitle:@"Error" message:@"Database not found. Created a new one" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlert show];
*/
        query = [NSString stringWithFormat:@"CREATE TABLE news_items (id integer primary key, nid integer, ntitle text, ntext text, ndate text);"];
        [manager executeQuery:query];
        query = [NSString stringWithFormat:@"CREATE TABLE event_items (id integer primary key, nid integer, ntitle text, ntext text, ndate text,calendar integer,status integer);"];
        [manager executeQuery:query];
        query = [NSString stringWithFormat:@"CREATE TABLE local_data (id integer primary key, key text, val text);"];
        [manager executeQuery:query];
        
        NSString *query = [NSString stringWithFormat:@"SELECT name FROM sqlite_master WHERE type='table';"];
        
        NSArray * arr = [manager loadDataFromDB:query];
        
        if([arr count]==3)
        {
            NSLog(@"db created!");
        }
        else
        {
            NSLog(@"db failed");
        }
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:screenBounds];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    
    self.nav = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    
    [window setRootViewController:  self.nav];
    
    self.nav.navigationBarHidden = true;
    
    [window makeKeyAndVisible];
    
    [self setWindow:window];
    
    query = [NSString stringWithFormat:@"SELECT * FROM local_data WHERE key='user';"];
    
    BOOL showLoginScreen = false;
    
    arr = [manager loadDataFromDB:query];
    NSInteger indexOfUsername = [self->manager.arrColumnNames indexOfObject:@"val"];
    if([arr count]==1)
    {
        if([[NSString stringWithFormat:@"%@", [[arr objectAtIndex:0] objectAtIndex:indexOfUsername]] isEqualToString:@"(null)"])
        {
            NSLog(@"user does not exist");
        }
        else
        {
            NSLog(@"user %@ is logged in", [NSString stringWithFormat:@"%@", [[arr objectAtIndex:0] objectAtIndex:indexOfUsername]]);
            username = [NSString stringWithFormat:@"%@", [[arr objectAtIndex:0] objectAtIndex:indexOfUsername]];
        }
    }
    else
    {
        NSLog(@"user does not exist");
        showLoginScreen = true;
    }
    
    query = [NSString stringWithFormat:@"SELECT * FROM local_data WHERE key='pw';"];
    
    arr = [manager loadDataFromDB:query];
    NSInteger indexOfPassword = [self->manager.arrColumnNames indexOfObject:@"val"];
    if([arr count]==1)
    {
        if([[NSString stringWithFormat:@"%@", [[arr objectAtIndex:0] objectAtIndex:indexOfPassword]] isEqualToString:@"(null)"])
        {
            NSLog(@"password does not exist");
        }
        else
        {
            NSLog(@"pw is %@", [NSString stringWithFormat:@"%@", [[arr objectAtIndex:0] objectAtIndex:indexOfPassword]]);
            password = [NSString stringWithFormat:@"%@", [[arr objectAtIndex:0] objectAtIndex:indexOfPassword]];
        }
    }
    else
    {
        NSLog(@"password not found");
        showLoginScreen = true;
    }
    
    query = [NSString stringWithFormat:@"SELECT * FROM local_data WHERE key='type';"];
    
    arr = [manager loadDataFromDB:query];
    NSInteger indexOfType = [self->manager.arrColumnNames indexOfObject:@"val"];
    if([arr count]==1)
    {
        if([[NSString stringWithFormat:@"%@", [[arr objectAtIndex:0] objectAtIndex:indexOfType]] isEqualToString:@"(null)"])
        {
            NSLog(@"type does not exist");
        }
        else
        {
            NSLog(@"type is %@", [NSString stringWithFormat:@"%@", [[arr objectAtIndex:0] objectAtIndex:indexOfType]]);
            type = [[NSString stringWithFormat:@"%@", [[arr objectAtIndex:0] objectAtIndex:indexOfType]] integerValue];
        }
    }
    else
    {
        NSLog(@"type not found");
        showLoginScreen = true;
    }
    
    if(showLoginScreen)
    {
        if(!loginScreenShown)
        {
            [self.viewController showLoginScreen];
            loginScreenShown = true;
        }
    }
    else
    {
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
        [request setHTTPMethod:@"POST"];
        NSString *postString = [NSString stringWithFormat:@"name=%@&pw=%@", [username urlencode],[password urlencode]];
        // NSLog(@"send token: %@", postString);
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];

    }

    
    return YES;
}

-(void)registerForNotifications
{
    //UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    
    //UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    
    if([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge];
    }
    /*#ifdef __IPHONE_8_0
    if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
#else
    //register to receive notifications
    UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
#endif
    */
    NSLog(@"registering....");
}

-(void)setCredentialsWithUsername:(NSString *)user andPassword:(NSString *)pw andType:(int)typ
{
    self->username = user;
    self->password = pw;
    self->type = typ;
    
    NSString * query = [NSString stringWithFormat:@"DELETE FROM local_data where key='user'"];
    // Execute the query.
    [manager executeQuery:query];
    query = [NSString stringWithFormat:@"DELETE FROM local_data where key='pw'"];
    // Execute the query.
    [manager executeQuery:query];
    query = [NSString stringWithFormat:@"DELETE FROM local_data where key='type'"];
    // Execute the query.
    [manager executeQuery:query];
    query = [NSString stringWithFormat:@"insert into local_data(id,key,val) values(null,'%@','%@')", @"user", user];
    // Execute the query.
    [manager executeQuery:query];
    
    query = [NSString stringWithFormat:@"insert into local_data(id,key,val) values(null,'%@','%@')", @"pw", pw];
    // Execute the query.
    [manager executeQuery:query];
    
    query = [NSString stringWithFormat:@"insert into local_data(id,key,val) values(null,'%@','%d')", @"type", typ];
    // Execute the query.
    [manager executeQuery:query];
    [self fetchNewData];
    [self loadEvents];
}

// Delegation methods
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    NSString* deviceToken = [[[[devToken description]
                                stringByReplacingOccurrencesOfString: @"<" withString: @""]
                               stringByReplacingOccurrencesOfString: @">" withString: @""]
                              stringByReplacingOccurrencesOfString: @" " withString: @""];
    self->registered = YES;
    [self sendProviderDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"Error in registration. Error: %@", err);
}

-(void) sendProviderDeviceToken:(NSString *)devTokenBytes
{
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"name=%@&pw=%@&type=register&devicetoken=%@&os=ios&usertype=%d", [username urlencode], [password urlencode],devTokenBytes,type];
   // NSLog(@"send token: %@", postString);
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection= [[NSURLConnection alloc] initWithRequest:request delegate:nil];
    NSLog(@"sending token...");
}

-(void)fetchNewData
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"name=%@&pw=%@&type=getnews", [username urlencode], [password urlencode]];
    // NSLog(@"send token: %@", postString);
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    self->conn= [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

-(void)loadEvents
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"name=%@&pw=%@&type=getevents", [username urlencode], [password urlencode]];
    // NSLog(@"send token: %@", postString);
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    self->conn= [[NSURLConnection alloc] initWithRequest:request delegate:self];

}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if ( application.applicationState == UIApplicationStateActive )
    {
        NSDictionary * alert = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        
        UIAlertController *alertv = [UIAlertController alertControllerWithTitle:[alert objectForKey:@"title"] message:[alert objectForKey:@"body"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        
        [alertv addAction:okButton];
        [self.viewController presentViewController:alertv animated:YES completion:nil];
        
    }

    
    [self fetchNewData];
    hasNewData = true;
    NSArray * vcs = [[[self.nav viewControllers] objectAtIndex:0] viewControllers];
    
    for(UINavigationController * uinav in vcs)
    {
        id<PushNotifications> pc = [[uinav childViewControllers] objectAtIndex:0];
        if([pc respondsToSelector:@selector(updateTableRows)])
        {
            [pc updateTableRows];
            
        }
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(self->connData==nil)
        self->connData = [[NSMutableData alloc] init];
    [self->connData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSError *localError = nil;

    NSString * str = [[NSString alloc] initWithData:connData encoding:NSISOLatin1StringEncoding];
    str = [str stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    connData = [[NSMutableData alloc] init];
    
    NSLog(@"%@",str);
    
    NSArray *results = [parsedObject valueForKey:@"events"];
    NSString * table;
    if([results count]>0)
        table = @"event_items";
    else
    {
        table = @"news_items";
        results = [parsedObject valueForKey:@"news"];
        NSLog(@"news received: %d",[results count]);
        
        if([results count]<=0 && [[[parsedObject valueForKey:@"authenticated"] stringValue] length]>0)
        {
            int authentication = [[parsedObject valueForKey:@"authenticated"] integerValue];
            if(authentication!=1)
            {
                if(!loginScreenShown)
                {
                    [self.viewController showLoginScreen];
                    loginScreenShown = true;
                }
            }
            else
            {
                if(loginScreenShown)
                {
                    [self.viewController hideLoginScreen];
                    loginScreenShown = false;
                }
            }
        }
    }
    
    NSMutableArray * checkedEventItems = [NSMutableArray arrayWithArray:[manager loadDataFromDB:@"SELECT * FROM event_items;"]];
    NSInteger indexOfId = [self->manager.arrColumnNames indexOfObject:@"nid"];
    
    for (NSDictionary *dic in results) {
        Item * item = [[Item alloc] init];
        
        [item setNid:[[dic valueForKey:@"id"] integerValue]];
        [item setNtitle:[dic valueForKey:@"title"]];
        [item setNtext:[dic valueForKey:@"text"]];
        [item setNdate:[dic valueForKey:@"date"]];
        [item setCal:[[dic valueForKey:@"calendar"] integerValue]];
        [item set_status:[[dic valueForKey:@"status"] integerValue]];
        if([[dic valueForKey:@"status"] isEqualToString:@""])
            [item set_status:1];
        else
            [item set_status:[[dic valueForKey:@"status"] integerValue]];
        
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE nid=%d;", table ,[item nid]];
        
        NSArray * arr = [manager loadDataFromDB:query];
        
        if([arr count]<=0)
        {
            // Prepare the query string.
            if([table isEqualToString:@"event_items"])
            {
                query = [NSString stringWithFormat:@"insert into %@ (nid,ntitle,ntext,ndate,calendar,status) values(%d, '%@', '%@','%@',%d,%d)", table,[item nid], [item ntitle], [item ntext], [item ndate], [item cal],[item _status]];
                
                for(int a = 0; a < [checkedEventItems count];a++)
                {
                    int nid = [[NSString stringWithFormat:@"%@", [[checkedEventItems objectAtIndex:a] objectAtIndex:indexOfId]] integerValue];
                    if(nid == [item nid])
                    {
                        [checkedEventItems removeObjectAtIndex:a];
                    //NSLog(@"remove from delete id: %d", nid);
                    }
                }
                
                [manager executeQuery:query];
            }
            else
            {
                query = [NSString stringWithFormat:@"insert into %@ (nid,ntitle,ntext,ndate) values(%d, '%@', '%@','%@')", table,[item nid], [item ntitle], [item ntext], [item ndate]];
                // Execute the query.
                [manager executeQuery:query];
            }
            
            
            // If the query was successfully executed then pop the view controller.
            if (manager.affectedRows != 0) {
                
            }
            else{
                NSLog(@"Could not execute the query.");
            }

        }
        else
        {
            if([table isEqualToString:@"event_items"])
            {
                query = [NSString stringWithFormat:@"UPDATE event_items SET ntitle='%@', ntext='%@',ndate='%@',status=%d WHERE nid=%d;",[item ntitle], [item ntext], [item ndate], [item _status],[item nid]];
                // Execute the query.
                [manager executeQuery:query];

                for(int a = 0; a < [checkedEventItems count];a++)
                {
                    int nid = [[NSString stringWithFormat:@"%@", [[checkedEventItems objectAtIndex:a] objectAtIndex:indexOfId]] integerValue];
                    if(nid == [item nid])
                    {
                        [checkedEventItems removeObjectAtIndex:a];
                        //NSLog(@"remove from delete id: %d", nid);
                    }
                }
            }
        }
        
        
    }
    
    NSArray * vcs = [[[self.nav viewControllers] objectAtIndex:0] viewControllers];
    
    if([table isEqualToString:@"event_items"] || [table isEqualToString:@"news_items"])
    {
        if([table isEqualToString:@"event_items"])
        {
            for(int a = 0; a < [checkedEventItems count];a++)
            {
                int nid = [[NSString stringWithFormat:@"%@", [[checkedEventItems objectAtIndex:a] objectAtIndex:indexOfId]] integerValue];
                NSString * query = [NSString stringWithFormat:@"DELETE FROM event_items WHERE nid=%d;" ,nid];
                [manager executeQuery:query];
            }
        }
        
        for(UINavigationController * uinav in vcs)
        {
            id<PushNotifications> pc = [[uinav childViewControllers] objectAtIndex:0];
            if([pc respondsToSelector:@selector(updateTableRows)])
            {
                if(([table isEqualToString:@"news_items"] && [pc isKindOfClass:[NewsTableViewController class]])
                   ||([table isEqualToString:@"event_items"] && [pc isKindOfClass:[EventTableViewController class]]))
                {
                
                    [pc updateTableRows];
                }
            
            }
        }
    }
    
    for(UINavigationController * uinav in vcs)
    {
        id<PushNotifications> pc = [[uinav childViewControllers] objectAtIndex:0];
        if([pc respondsToSelector:@selector(updateTableRows)])
        {
            [pc updateTableRows];
        }
    }
    
}

-(NSString *)getUser
{
    return username;
}

-(void)sendPositionWithLatitude:(float)latitude andLongitude:(float)longitude
{
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:APP_URL]];
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"name=%@&pw=%@&type=updatelocation&lon=%f&lat=%f", [username urlencode], [password urlencode],longitude,latitude];
 
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    self->conn= [[NSURLConnection alloc] initWithRequest:request delegate:nil];

}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    application.applicationIconBadgeNumber = 0;

    if(hasNewData)
    {
        [self fetchNewData];
        hasNewData = false;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
