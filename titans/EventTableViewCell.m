//
//  NewsTableViewCell.m
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "EventTableViewCell.h"
#import "EventTableViewController.h"

@implementation EventTableViewCell

@synthesize title,subTitle, hasDetail, join, join_label, index,cal;

- (void)awakeFromNib {
    
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        CGSize screen = [[UIScreen mainScreen] bounds].size;
        
        //[self setBackgroundColor:[UIColor colorWithRed:255 green:0 blue:0 alpha:255]];
        //[self setAlpha:0.1f];
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, screen.width/2, 30)];
        [self.title setTextColor:[UIColor blackColor]];
        [self.title setFont:[UIFont fontWithName:@"Arial" size:18.0f]];
        [self addSubview:self.title];
        self.title.numberOfLines = 2;
        self.title.minimumScaleFactor = 0.7;
        self.title.adjustsFontSizeToFitWidth = YES;

        self.subTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 45, screen.width/2, 30)];
        [self.subTitle setTextColor:[UIColor blackColor]];
        [self.subTitle setTextAlignment:NSTextAlignmentLeft];
        [self.subTitle setFont:[UIFont fontWithName:@"Arial" size:14.0f]];
        [self addSubview:self.subTitle];
        
        self.join = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"✓",@"", @"✗", nil]];
        [self.join setFrame:CGRectMake(screen.width-140, 20, 100, 50)];
        [self.join addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventAllEvents];
        [self.join setSelected:YES];
            [self addSubview:self.join];
        [self.join setWidth:0.001 forSegmentAtIndex:1];
        
        
        id okObject = [[self.join subviews] objectAtIndex:0];
        UIColor *tintcolor=[UIColor colorWithRed:255.0 green:153.0/255.0 blue:0/255.0 alpha:1.0];
        [okObject setTintColor:tintcolor];
        id middleObject = [[self.join subviews] objectAtIndex:1];
        
        id notOkObject = [[self.join subviews] objectAtIndex:2];
        [notOkObject setTintColor:tintcolor];
        [middleObject setTintColor:tintcolor];
        
        
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Helvetica" size:22.0f], NSFontAttributeName,
                                    [UIColor orangeColor], NSForegroundColorAttributeName, nil];
        
        [self.join setTitleTextAttributes:attributes forState:UIControlStateNormal];
        
   /*     self.join_label = [[UILabel alloc] initWithFrame:CGRectMake(screen.width-150, 45, 100, 30)];
        [self.join_label setTextColor:[UIColor blackColor]];
        [self.join_label setTextAlignment:NSTextAlignmentRight];
        [self.join_label setFont:[UIFont fontWithName:@"Arial" size:14.0f]];
        [self.join_label setText:@"bin dabei!"];
        [self addSubview:self.join_label];*/
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) switchChanged:(id)sender
{
    
    NSLog(@"selected: id: %d, status: %d",index,[sender selectedSegmentIndex]);
    if([sender selectedSegmentIndex]==1)
    {
        if(cstatus==0)
            [sender setSelectedSegmentIndex:0];
        else
            [sender setSelectedSegmentIndex:2];
        
        return;
    }

    
    if([sender selectedSegmentIndex]==0)
    {
        cstatus = 0;
    }
    else
    {
        cstatus = 2;
    }
    [self refresh];
    [self.delegate cellDidTap:self withCheck:cstatus];
}

-(void)setStatus:(int)status
{
    [self.join setSelectedSegmentIndex:status];
}

-(int)cstatus
{
    return self.cstatus;
}

-(void)refresh
{
    if(hasDetail)
    {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
    
   /* if(status==0)
    {
        self.join_label.text = @"bin dabei!";
        

    }
    else if(status==2)
    {
        self.join_label.text = @"kann nicht.";
    }
    else
    {
        self.join_label.text = @"noch offen";
    }*/
}

@end
