//
//  Item.h
//  titans
//
//  Created by Severin Zumbrunn on 05.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject

@property (nonatomic,retain) NSString * ntitle;
@property (nonatomic,retain) NSString * ntext;
@property (nonatomic,retain) NSString * ndate;
@property int _status;
@property int nid;
@property int cal;
@property BOOL isEvent;

@end
