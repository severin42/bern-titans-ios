//
//  UserAnnotation.m
//  titans
//
//  Created by Severin Zumbrunn on 15.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "UserAnnotation.h"

@implementation UserAnnotation

@synthesize coordinate,title,subtitle,category,people;

@end
