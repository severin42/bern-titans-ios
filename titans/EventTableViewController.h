//
//  EventTableViewController.h
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsTableViewController.h"
#import "EventTableViewCell.h"
#import "EventObject.h"

#define kCellHeight 44

@interface EventTableViewController : UITableViewController <PushNotifications, UITableViewDelegate, myUITableViewCellDelegate,NSURLConnectionDelegate>
{
    NSArray * items;
    DBManager * manager;
    UILabel *messageLabel;
    NSMutableArray * changesWaitingForConfirmation;
    NSMutableData * connData;
    UIImageView * bgView;
}

-(void)removeOldObjectsFromDB;
-(void)loadObjectsFromDB;
-(void)editFilter:(id)sender;
-(void) setEvent:(int)event_id withCheck:(int)joined;

@end
