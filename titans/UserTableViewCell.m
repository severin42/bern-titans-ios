//
//  UserTableViewCell.m
//  titans
//
//  Created by Severin Zumbrunn on 03.05.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "UserTableViewCell.h"

@implementation UserTableViewCell

@synthesize name,status;

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        CGSize screen = [[UIScreen mainScreen] bounds].size;
        [self setBackgroundColor:[UIColor clearColor]];
        self.name = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, screen.width-50, 40)];
        [self.name setTextColor:[UIColor blackColor]];
        [self.name setFont:[UIFont fontWithName:@"Arial" size:18.0f]];
        [self addSubview:self.name];
        self.name.numberOfLines = 1;
        self.name.minimumScaleFactor = 0.7;
        self.name.adjustsFontSizeToFitWidth = YES;
        self.status = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"✓",@"", @"✗", nil]];
        [self.status setFrame:CGRectMake(screen.width-70, 7, 50, 30)];
        [self.status setSelected:YES];
        [self addSubview:self.status];
        [self.status setWidth:0.001 forSegmentAtIndex:1];
        [self.status setUserInteractionEnabled:false];
        
        id okObject = [[self.status subviews] objectAtIndex:0];
        UIColor *tintcolor=[UIColor colorWithRed:255.0 green:153.0/255.0 blue:0/255.0 alpha:1.0];
        [okObject setTintColor:tintcolor];
        id middleObject = [[self.status subviews] objectAtIndex:1];
        
        id notOkObject = [[self.status subviews] objectAtIndex:2];
        [notOkObject setTintColor:tintcolor];
        [middleObject setTintColor:tintcolor];
        
    }
    return self;
}

-(void)setStatusForValue:(int)val
{
    [self.status setSelectedSegmentIndex:val];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
