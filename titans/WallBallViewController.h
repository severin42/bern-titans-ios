//
//  WallBallViewController.h
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "NewsTableViewController.h"
#import "AppDelegate.h"
#import "UserAnnotation.h"
#import "MapAnnotationViewController.h"
#import "AnnotationObject.h"


@interface WallBallViewController : UIViewController<PushNotifications,CLLocationManagerDelegate, NSURLConnectionDelegate,UIGestureRecognizerDelegate,MKMapViewDelegate>
{
    CLLocationManager *locationManager;
    MKMapView *gmap;
    float latitude;
    float longitude;
    NSURLConnection * conn;
    NSMutableData * connData;
}

-(void)passPosition:(NSTimer *) timer;

-(void)putNewAnnotation:(AnnotationObject *) object;

-(void)updateTableRows;

@end
