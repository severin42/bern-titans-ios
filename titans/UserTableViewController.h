//
//  UserTableViewController.h
//  titans
//
//  Created by Severin Zumbrunn on 03.05.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "UserTableViewCell.h"

@interface UserTableViewController : UITableViewController
{
    
}

@property(nonatomic,retain) NSArray * users;

-(void)updateTableWithArray:(NSArray *) array;

@end
