//
//  ViewController.m
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "ViewController.h"
#import "NewsTableViewController.h"
#import "EventTableViewController.h"
#import "WallBallViewController.h"
#import "LoginViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabBarController.tabBar.tintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.translucent = false;
    
    NewsTableViewController * ntvc = [[NewsTableViewController alloc] initWithNibName:@"NewsTableViewController" bundle:nil];
    
    EventTableViewController * etvc = [[EventTableViewController alloc] initWithNibName:@"EventTableViewController" bundle:nil];
    UINavigationController * nav1 = [[UINavigationController alloc] init];
    [nav1 addChildViewController:ntvc];
    UITabBarItem * nit = [[UITabBarItem alloc] initWithTitle:@"News" image:[UIImage imageNamed:@"newspaper-7.png"] tag:0];
    [ntvc setTabBarItem:nit];
    UINavigationController * nav2 = [[UINavigationController alloc] init];
    [nav2 addChildViewController:etvc];
    UITabBarItem * eit = [[UITabBarItem alloc] initWithTitle:@"Events & Training" image:[UIImage imageNamed:@"calendar-7.png"] tag:1];
    [etvc setTabBarItem:eit];

    self.viewControllers = [NSArray arrayWithObjects:nav1,nav2, nil];
    self.view.autoresizingMask=(UIViewAutoresizingFlexibleHeight);
    self.view.backgroundColor = [UIColor clearColor];
    
    self->manager = [[DBManager alloc] initWithDatabaseFilename:@"titansdb.sql"];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) showLoginScreen
{
    NSLog(@"Showing login controller...");
    LoginViewController * lvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    [self.navigationController pushViewController:lvc animated:NO];
    
    
}

-(void) hideLoginScreen
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) refresh:(id)sender
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
