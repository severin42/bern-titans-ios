//
//  UserTableViewController.m
//  titans
//
//  Created by Severin Zumbrunn on 03.05.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import "UserTableViewController.h"

@interface UserTableViewController ()

@end

@implementation UserTableViewController

@synthesize users;

- (void)viewDidLoad {
    [super viewDidLoad];
    CGSize screen = [[UIScreen mainScreen] bounds].size;
    users = [[NSArray alloc] init];
    [self.view setFrame:CGRectMake(0, screen.height/2, screen.width, screen.height/2)];
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateTableWithArray:(NSArray *)array
{
    users = [[NSArray alloc] initWithArray:array];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [users count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * CellIdentifier = @"UserTableViewCell";
    UserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil)
    {
        cell = [[UserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [cell.name setText:[[users objectAtIndex:[indexPath row]] name]];
    [cell setStatusForValue:[[users objectAtIndex:[indexPath row]] _status]];
    
    return cell;
}

@end
