//
//  NewsTableViewCell.h
//  titans
//
//  Created by Severin Zumbrunn on 03.04.15.
//  Copyright (c) 2015 tune-X. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol myUITableViewCellDelegate

-(void) cellDidTap:(id) sender withCheck:(int)checked;
@end

@interface EventTableViewCell : UITableViewCell
{
    int cstatus; // 0 = yes, 1 = unknown, 2 = false
}

@property (nonatomic,retain) UILabel * title;
@property (nonatomic,retain) UILabel * subTitle;

@property BOOL hasDetail;
@property (nonatomic,retain) UISegmentedControl * join;
@property (nonatomic, retain) UILabel * join_label;
@property int index;
@property int cal;

@property (nonatomic, assign) id<myUITableViewCellDelegate> delegate;

-(void) refresh;
- (void) switchChanged:(id)sender;
-(int) cstatus;
-(void) setStatus:(int)cstatus;
@end